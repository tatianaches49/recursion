﻿using System;

namespace Rec
{
    class Program
    {
        // Простым числом называют такое натуральное число, которое больше 1 и делится только на 1 и само на себя.

        static bool check = true;
        static int k = 2;
        static int n; // вводимое число
        static void Main(string[] args)
        {
            Console.Write("Введите число: ");
            n = int.Parse(Console.ReadLine()); // считывание
            Func(k); // вызов функции

            if (check) // результат
            {
                Console.WriteLine("Число простое");
            }
            else
            {
                Console.WriteLine("Число не простое");
            }
        }

        private static void Func(int k)
        {
            if (k < (n / 2))
                if (n % k == 0)
                    check = false;
                else
                {
                    k++;
                    Func(k);
                }
        }
    }
}
