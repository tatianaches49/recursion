﻿using System;

namespace One
{
    class Program
    {
        static int n;
     static void Recursion(int i, int pos, int sum, int[] a)
        {
            for (int c = i; c < n; c++)
            {
                a[pos] = c;
                if (sum + c == n) // совпадение
                {
                    for (int j = 0; j < pos; j++) // вывод суммы чисел
                    {
                        Console.Write(a[j] + "+"); 
                    }
                    Console.WriteLine(a[pos]);
                }
                else if (sum + c < n)
                {
                    Recursion(c, pos + 1, sum + c, a);
                }
                else break;
            }
        }
        static void Main(string[] args)
        {
            
            n = int.Parse(Console.ReadLine()); // получение числа с консоли
            int[] a = new int[1]; // создание массива
            Array.Resize(ref a, n); // увеличение размера ref a - подлежащий изменению размера одномерный массив, n - pазмер нового массива
            Recursion(1, 0, 0, a); // вызов функции
            
        }

    
    }
}
