﻿using System;

namespace Three
{
    class Program
    {
        static void Main(string[] args)
        {
            string stroka = Console.ReadLine(); // получение строки
            Console.WriteLine(IsPalindrom(stroka.ToLower())); // вывод результата
       
        }

        public static bool IsPalindrom(string stroka)
        {
            if (null == stroka)
                return true; 

            if (stroka.Length <= 1)
                return true;
            else
                return (stroka[0] == stroka[stroka.Length - 1]) && (IsPalindrom(stroka.Substring(1, stroka.Length - 2)));
        }

    }
}

